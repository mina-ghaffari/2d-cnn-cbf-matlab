function [BrainPatch , LabelPatch,Label_index, num_patches] = ReadBrainScan (BrainScan, Mask)

    Volume= niftiread (BrainScan);
    mask = niftiread (Mask);

    mask (mask ~=0) = 0.5;
    mask (mask ==0) = 1;
    
    stroke_volume = Volume .* mask;
    
    % to generate all possible patches out of the volume with lesion
    Patch_size = 13;
    cnt_i = size (stroke_volume, 1)- Patch_size+1;  % all possible patches while scanning horizentally
    cnt_j = size (stroke_volume, 2)-Patch_size+1;  % all possible patches while scanning vertically

    n_patch = cnt_i*cnt_j* size (Volume, 3);
    idx = randperm(n_patch , floor(0.93 * n_patch) );


    cnt_zero = 0;
    cnt_aug  = 0;
    for k =1:size (stroke_volume, 3)
      for j= 1 : cnt_j
         for i= 1 : cnt_i
             cnt_batch = (k-1) * cnt_i * cnt_j + (j-1)* cnt_i + i-cnt_zero +cnt_aug;
             cnt_index = (k-1) * cnt_i * cnt_j + (j-1)* cnt_i + i;
             
                  BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch) = stroke_volume (i:i+Patch_size-1, j:j+Patch_size-1, k);
                  LabelPatch (cnt_batch, 1) = mask (i +(Patch_size-1)/2, j+(Patch_size-1)/2,k); % the middle pixel in each patchis 
                  Label_index(cnt_batch, 1) = cnt_index;
                  
                  if ((nnz(BrainPatch(:,:,cnt_batch)) ==0)  )
                     cnt_zero = cnt_zero+1;
                  elseif ( (LabelPatch (cnt_batch,1) == 1 ) && (ismember(cnt_index, idx)) )
                     cnt_zero = cnt_zero+1;
                  elseif ( LabelPatch (cnt_batch,1) ~= 1)
                      BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +1) = rot90 (BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch ));
                      LabelPatch (cnt_batch +1 ,1) = 0;
                      Label_index(cnt_batch +1 ,1) = cnt_index;
                      
                      BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +2) = imnoise( BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +1) ,  'gaussian');
                      LabelPatch (cnt_batch +2 ,1) = 0;
                      Label_index(cnt_batch +2 ,1) = cnt_index; 
                      
                      
                      
                      BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +3) = rot90 (BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch+1 ));
                      LabelPatch (cnt_batch +3 ,1) = 0;
                      Label_index(cnt_batch +3 ,1) = cnt_index;
                      
                      BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +4) = imnoise( BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +3) ,  'gaussian');
                      LabelPatch (cnt_batch +4 ,1) = 0;
                      Label_index(cnt_batch +4 ,1) = cnt_index; 
                      
                      
                      
                      BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +5) = rot90 (BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +3 ));
                      LabelPatch (cnt_batch +5 ,1) = 0;
                      Label_index(cnt_batch +5 ,1) = cnt_index;  
                      
                      BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +6) = imnoise( BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +5) ,  'gaussian');
                      LabelPatch (cnt_batch +6 ,1) = 0;
                      Label_index(cnt_batch +6 ,1) = cnt_index; 
                      
                      
                      
                      
                      
                      BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +7) = flipud (BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch  ));
                      LabelPatch (cnt_batch + 7 ,1) = 0;
                      Label_index(cnt_batch + 7 ,1) = cnt_index;                       
                      
                      
                      BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +8) = imnoise( BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +7) ,  'gaussian');
                      LabelPatch (cnt_batch +8 ,1) = 0;
                      Label_index(cnt_batch +8 ,1) = cnt_index; 
                      
                      
                      
                      BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +9) = fliplr (BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch  ));
                      LabelPatch (cnt_batch +9 ,1) = 0;
                      Label_index(cnt_batch +9 ,1) = cnt_index; 
                      
                      
                      BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +10) = imnoise( BrainPatch(1:Patch_size, 1:Patch_size , cnt_batch +9) ,  'gaussian');
                      LabelPatch (cnt_batch +10 ,1) = 0;
                      Label_index(cnt_batch +10 ,1) = cnt_index; 
                      
                                           
                   
                      cnt_aug = cnt_aug+10;
                  end
         end
      end
    end   
    
    LabelPatch (LabelPatch(:,1) ~= 1) = 0;
    num_patches = size (LabelPatch , 1) - cnt_aug + cnt_zero;
   
    
    
    % getiing rid of patches labeled as 1 in a random fasion 
    
    

    

end


