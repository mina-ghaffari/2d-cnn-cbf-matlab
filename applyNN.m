function [net , y_predicted ] =  applyNN ( patch_size, kernel_size,  n_layer_1 , n_layer_2, n_layer_3, x_train, y_train, x_validation , y_validation , x_test)

%Network configuration
layers = [
          imageInputLayer([patch_size patch_size 1])
          
          convolution2dLayer(kernel_size,n_layer_1 ,'Padding',1)
          batchNormalizationLayer
          reluLayer
          
          maxPooling2dLayer(2,'Stride',2)
          
          convolution2dLayer(kernel_size,n_layer_2,'Padding',1)
          batchNormalizationLayer
          reluLayer
          
          maxPooling2dLayer(2,'Stride',2)
          
          convolution2dLayer(kernel_size,n_layer_3,'Padding',1)
          batchNormalizationLayer
          reluLayer
          
          fullyConnectedLayer(2)
          softmaxLayer
          classificationLayer];




options = trainingOptions('sgdm', ...            %SGDM stands for the stochastic gradient descent with momentum solver
                          'MaxEpochs',100, ...
                          'ValidationData',{x_validation, y_validation}, ...
                          'ValidationFrequency',1000, ...              %Choose the 'ValidationFrequency' value so that the network is validated about once per epoch
                          'ValidationPatience',5, ...
                          'ExecutionEnvironment','cpu',...
                          'Verbose',false,...
                          'Plots','training-progress')


%train network and predict the labels             
net = trainNetwork(x_train, y_train,layers,options);

y_predicted = classify(net,x_test);



end