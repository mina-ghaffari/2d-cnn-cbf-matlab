function [Augmented_lesion] = augment_data (lesion_patches , ratio)    % rotate 90, 180 and 270 degree. add noise to all rotations. flip up-down. flip left-right

if (ratio > 7)
    augmentation_methods = 7; 
    ratio = 7;
else
    augmentation_methods = ratio;
end
    
cnt_patch =0;
Augmented_lesion = zeros (size(lesion_patches ,1), size(lesion_patches ,2) , augmentation_methods *size(lesion_patches ,3));
for i = 1 : size (lesion_patches, 3)
    
        Augmented_lesion (:,:,cnt_patch + i) = lesion_patches (:,:,i);
     if (ratio > 1)
        Augmented_lesion (:,:,cnt_patch +i+1) = imnoise( lesion_patches  (:,:,i) ,  'gaussian');
     end
     
     if (ratio > 2)
        Augmented_lesion (:,:,cnt_patch +i+2) = flipud (lesion_patches (:,:,i));
     end
     
     if (ratio > 3)  
        Augmented_lesion (:,:,cnt_patch +i+3) = fliplr (lesion_patches (:,:,i));
     end
     
     if (ratio > 4)       
        Augmented_lesion (:,:,cnt_patch +i+4) = rot90 (lesion_patches (:,:,i));
     end
     
     if (ratio > 5)
        Augmented_lesion (:,:,cnt_patch +i+5) = rot90 (lesion_patches (:,:,i) ,2);
     end
     
    if (ratio > 6)
        Augmented_lesion (:,:,cnt_patch +i+6) = rot90 (lesion_patches (:,:,i),3);
    end
    
   cnt_patch  = cnt_patch + augmentation_methods -1;
end


end
