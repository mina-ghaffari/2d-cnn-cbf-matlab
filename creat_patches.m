function [normal_patches , lesion_patches ,ratio ] = creat_patches (Volume, Mask, Patch_size )   


    Mask (Mask ~=0) = 0.5;
    Mask (Mask ==0) = 1;
    
    lesion_volume = Volume .* Mask;
    
    cnt_i = size (lesion_volume, 1)- Patch_size+ 1;  % all possible patches while scanning horizentally
    cnt_j = size (lesion_volume, 2)- Patch_size+ 1;  % all possible patches while scanning vertically

   
    
    
    cnt_lesion = 1;
    cnt_normal = 1;
    for k =1:size (lesion_volume, 3)
      for j= 1 : cnt_j
         for i= 1 : cnt_i
             
                        
             if (nnz (lesion_volume (i:i+Patch_size-1, j:j+Patch_size-1, k)) ~= 0)

                 BrainPatch (1:Patch_size, 1:Patch_size ) = lesion_volume (i:i+Patch_size-1, j:j+Patch_size-1, k);
                 LabelPatch  = Mask (i +(Patch_size-1)/2, j+(Patch_size-1)/2,k); % the middle pixel in each patchis 
                        
                 if ( LabelPatch  == 1)
                     normal_patches (1:Patch_size, 1:Patch_size , cnt_normal)= BrainPatch;
                     cnt_normal = cnt_normal +1;
                 else
                     lesion_patches (1:Patch_size, 1:Patch_size , cnt_lesion)= BrainPatch;
                     cnt_lesion = cnt_lesion +1;
                 end
                 
             end 
             
         end
      end
    end
    
    ratio = floor (size( normal_patches,3) / size(lesion_patches,3));
      
    
end
                  
             
             
             
             
