function [total_patches , LabelPatch, patch_indices  ] = creat_patches_testdata (brainscan, mask, Patch_size , filter_size, filter_threshold)  

    Volume_noisy = niftiread (brainscan);
    Mask = niftiread (mask);
    Mask_in = niftiread (mask);

    Volume = denoise_brainscan (Volume_noisy, filter_size, filter_threshold);
%     Volume = Volume_noisy ;
    
    
    Mask (Mask ~=0) = 0.5;
    Mask (Mask ==0) = 1;
    
    lesion_volume = Volume .* Mask;


    cnt_i = size (lesion_volume, 1)- Patch_size+ 1;  % all possible patches while scanning horizentally
    cnt_j = size (lesion_volume, 2)- Patch_size+ 1;  % all possible patches while scanning vertically

   
    
    
    cnt_patch = 1;
    for k =1:size (lesion_volume, 3)
      for j= 1 : cnt_j
         for i= 1 : cnt_i
             
             if (nnz (lesion_volume (i:i+Patch_size-1, j:j+Patch_size-1, k)) > 0)

                 total_patches (1:Patch_size, 1:Patch_size , cnt_patch)= lesion_volume (i:i+Patch_size-1, j:j+Patch_size-1, k);
                 patch_indices (cnt_patch , 1) = (k-1) * cnt_i * cnt_j + (j-1)* cnt_i + i ;
                 LabelPatch (1,  cnt_patch) = Mask_in (i +(Patch_size-1)/2, j+(Patch_size-1)/2,k); % the middle pixel in each patchis 

                 cnt_patch = cnt_patch +1;
                 
             end       
             
         end
      end
    end
    
    total_patches = single(reshape(total_patches,13,13,1,[]));
    idx = randperm ( size (total_patches , 4)); 
    total_patches = total_patches (:,:,:, idx);
    patch_indices = patch_indices(idx, :);    
    
    LabelPatch = LabelPatch (:, idx);

    
    
end

