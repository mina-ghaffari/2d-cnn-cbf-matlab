function [denoised_Volume] = denoise_brainscan (noisy_volume, filter_size, filter_threshold)


cnt_i = size (noisy_volume, 1)- filter_size+ 1;  % all possible patches while scanning horizentally
cnt_j = size (noisy_volume, 2)- filter_size+ 1;  % all possible patches while scanning horizentally

denoised_Volume = noisy_volume;
for i=1:cnt_i 
    for j=1:cnt_j 
        for k=1: size(noisy_volume, 3)
            if ( noisy_volume (i+1,j+1, k ) > filter_threshold )
                 denoised_Volume (i+(filter_size-1)/2 ,j+ (filter_size-1)/2, k ) = sum ( sum ( noisy_volume (i:i+ filter_size -1 , j:j+ filter_size -1 ,k) < filter_threshold )) / numel (noisy_volume (i:i+ filter_size -1 , j:j+ filter_size -1 ,k)< filter_threshold);
            end
        end
    end
end

end
    
