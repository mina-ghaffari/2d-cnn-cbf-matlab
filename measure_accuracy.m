function [Accuracy, Sensitivity, Specificity , Dice_score] = measure_accuracy (expected, predictid)


mask_original = niftiread (expected);
mask_predicted = niftiread (predictid);

mask_original_vectorized = reshape (mask_original , [], 1);
mask_pred_vectorized = reshape (mask_predicted , [], 1);

Confusion_Matrix = confusionmat (mask_original_vectorized , mask_pred_vectorized)
TN = Confusion_Matrix(1,1);
FP = Confusion_Matrix (1,2);
FN = Confusion_Matrix (2,1);
TP = Confusion_Matrix (2,2);

Accuracy = (TP+TN)/ (TP+TN+FP+FN);
Sensitivity = TP/ (TP+FN);
Specificity = TN / (TN+FP);


Dice_score = (2*TP)/ (2*TP+ FP + FN);


% Dice_score = dice ( double (mask_predicted) , double(mask_predicted));

end