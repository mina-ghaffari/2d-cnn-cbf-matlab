function[num_parameters] = n_learnable_parameters (kernel_size, n_layer_1 , n_layer_2, n_layer_3)

n_kernel = kernel_size * kernel_size ; 
n_convolutional =   (  n_kernel * 1 +1)* n_layer_1 + (  n_kernel * n_layer_1 +1)* n_layer_2 + (  n_kernel * n_layer_2 +1)* n_layer_3 ;
n_fullyconnected = (n_layer_3 * 13 *13 +1 ) * 2 ;   % 13 = size of each patch after passing through the last convolutional layer,   2: number of classes
num_parameters = n_convolutional + n_fullyconnected ;
end