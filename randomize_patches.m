function [total_patches , total_labels] = randomize_patches (num_brainscans, num_start, patch_size , filter_size , filter_threshold , normal_to_lesion_ratio)


total_patches =[];
total_labels = [];
for i =1: num_brainscans   
      BrainScan = niftiread (sprintf('%d.nii',i + num_start));  
      Label     = niftiread (sprintf('%d_mask.nii.gz',i+ num_start));
      
      % preprocessing 
      denoised_Volume = denoise_brainscan (BrainScan, filter_size, filter_threshold);
%         denoised_Volume = BrainScan ; 


      [normal_patches , lesion_patches ,ratio ] = creat_patches (denoised_Volume, Label, patch_size );
      Augmented_lesion = augment_data  (lesion_patches , ratio);
      test = floor (size (normal_patches, 3) / size (Augmented_lesion ,3));
      Reduced_patches = reduce_patches (normal_patches , normal_to_lesion_ratio * size (Augmented_lesion,3));
      
      %concatenate all patches from all brain scans
      total_patches = cat( 3, total_patches , Reduced_patches , Augmented_lesion );
      total_labels  = cat( 2, total_labels , zeros(1, size (Reduced_patches , 3)) , ones(1, size (Augmented_lesion , 3)));         
end 

%randomize patch indices order

total_patches = single(reshape(total_patches,13,13,1,[]));

idx = randperm(size (total_patches , 4) );
total_patches = total_patches(:,:,:, idx);

total_labels = reshape (total_labels,1,[]);
total_labels = total_labels(:,idx);

total_labels = categorical(total_labels);     % categorical labels for MATLAB NN




end