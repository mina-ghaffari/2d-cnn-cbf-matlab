function [mask]= reconstruct_mask (Y_predicted, Label_Index, Patch_size )

mask = zeros (64,64,20);
cnt_i = size (mask, 1)- Patch_size+1;  
cnt_j = size (mask, 2)- Patch_size+1;


i=0;
j=0;
k=0;
for l = 1: size(Label_Index)
    
    
        if ( mod (Label_Index(l) , cnt_i*cnt_j) == 0 )
           k =  Label_Index(l) / (cnt_i *cnt_j);
        else 
           k = floor (Label_Index(l) / (cnt_i *cnt_j)) +1;
        end

          k_res = Label_Index(l) - (k-1)* (cnt_i*cnt_j);

        if ( mod (k_res , cnt_i) == 0 )
          j =  k_res/ cnt_i;
        else
          j =  floor (k_res/ cnt_i)+1;
        end

        i = k_res - (j-1)* cnt_i;
        
        mask (i +(Patch_size-1)/2, j+(Patch_size-1)/2,k) =  Y_predicted (l,1);
        
end


end