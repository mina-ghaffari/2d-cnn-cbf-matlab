function [reduced_patches] = reduce_patches(normal_patches , normal_to_lesion_ratio)  %reduction_rate  a float : 0-1

    n_patches = size (normal_patches ,3);
    idx = randperm(n_patches ,  normal_to_lesion_ratio );

    cnt_patch = 1 ;
    for i =1:n_patches
        if (ismember(i, idx))
            reduced_patches (:,:, cnt_patch ) = normal_patches (:,:,i);
            cnt_patch = cnt_patch + 1;
        end
    end

end