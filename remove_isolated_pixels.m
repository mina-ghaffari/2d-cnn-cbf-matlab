function [mask_out] = remove_isolated_pixels (mask_in)
mask_out = single(mask_in);
 for i=2: size(mask_in, 1)-1
     for j= 2: size (mask_in, 2)-1
         for k= 2: size(mask_in, 3)-1
              if (nnz (mask_in (i-1:i+1, j-1:j+1, k-1:k+1)) == 1 )   % if the pixeld: mask (i, j, k) does not have any neighbours
               mask_out (i, j, k) = 0;               
             end
         end
     end
 end
 
  mask_out (mask_out == 1) = 0;
  mask_out (mask_out == 2) = 1;

 
end