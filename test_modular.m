% Initialize variables
patch_size = 13;
filter_size = 3;              % filter to denoise the brain scan
filter_threshold = 150;




normal_to_lesion_ratio = 1;




n_layer_1 = 20;               % Number of filters in each layer of the CNN
n_layer_2 = 50;
n_layer_3 = 90;
kernel_size = 3;



% prepare inputs for training
[x_train , y_train] = randomize_patches (11, 0, patch_size , filter_size , filter_threshold, normal_to_lesion_ratio );
[x_val , y_val]     = randomize_patches (1, 11, patch_size , filter_size , filter_threshold, normal_to_lesion_ratio );
[x_test , y_test, patch_indices  ] = creat_patches_testdata ('13.nii', '13_mask.nii.gz' , patch_size , filter_size, filter_threshold);


% tarin a CNN and predict the outputs
[net , y_predicted ] =  applyNN ( patch_size, kernel_size, n_layer_1 , n_layer_2, n_layer_3, x_train, y_train, x_val , y_val , x_test);


%reconstruct a 3D mask from the predicted vector
mask_predicted = reconstruct_3Dmask (y_predicted, patch_indices, patch_size );


% remove isolated pixels
mask = remove_isolated_pixels (mask_predicted);



%generate nifti file from mask
niftiwrite (mask,'13_pred.nii', niftiinfo('13_mask.nii.gz'));


% Accuracy , Sensitivity and Specificity calculation
[Accuracy, Sensitivity, Specificity, Dice_score] = measure_accuracy ('13_mask.nii.gz', '13_pred.nii');



% calculate the number of learnable parameters
[num_parameters] = n_learnable_parameters (kernel_size, n_layer_1 , n_layer_2, n_layer_3)